# User Story Copilot - MVP

## Disclaimer

It is not production ready and is not intended for use in a production environment. 


## Overview
User Story Copilot is an MVP designed to assist in the understanding and analysis of user stories within software projects. It leverages language models and embeddings to provide insights into the source code and related documentation.

## Dependencies
Before starting, ensure you have a Python environment set up. It is recommended to use a virtual environment (`venv`). Install the required dependencies using pip:

```bash
virtualenv venv   
source venv/bin/activate
pip3 install flask pandas langchain langchain-openai langchain-mistralai langchain-community==0.0.19 tokeniser flask-sqlalchemy pyarrow pydantic chromadb 
```

### Getting Started
To use the User Story Copilot, follow these steps:

1. **Clone Project Repositories**: Clone your project repositories containing the source code into the `/projects/` directory of this application. Ensure each project you wish to analyze has its own subdirectory within `/projects/`.

2. **Environment Variables**: Set the `OPENAI_API_KEY` (and `MISTRAL_API_KEY` if needed) environment variable with your OpenAI API key to enable embeddings generation and other AI-driven features.

3. **Generate Embeddings**: Run `embeddings.py` to process the source code in the `/projects/` directory. This script generates vector embeddings for each project and stores them in the `cache` folder for later use.

4. **Start the Server**: Run `server.py` to start the Flask server. Once the server is running, you can access the User Story Copilot UI through your local browser by navigating to the server's address (typically `http://localhost:5000/`).

5. **UI Interaction**: Use the simple UI to input user stories, select projects, and generate analyses. The UI provides a minimalistic interface for interacting with the underlying AI models.

## Additional Information
- The application currently supports basic text processing and AI-driven analysis. Expect limited functionality and performance at this stage.

- Ensure that your project repositories are structured in a way that the application can process. The current version expects Python and Markdown files but can be extended to support more file types and languages.

## Feedback and Contributions
If you have suggestions, improvements, or encounter any issues, please feel free to contribute to the project or open an issue.
