import os
import logging
from langchain_community.document_loaders.generic import GenericLoader
from langchain_community.vectorstores import Chroma
from langchain.embeddings import CacheBackedEmbeddings
from langchain.storage import LocalFileStore
from langchain_openai import OpenAIEmbeddings
from langchain.text_splitter import Language, RecursiveCharacterTextSplitter
from langchain_community.document_loaders.parsers import LanguageParser

def count_files_recursive(directory, extension=".py"):
    count = 0
    for root, _, files in os.walk(directory):
        count += sum(file.endswith(extension) for file in files)
    return count

def setup_logging():
    logging.basicConfig(level=logging.INFO)

def main():
    setup_logging()

    api_key_openai = os.getenv("OPENAI_API_KEY")
    embeddings_model = "text-embedding-3-large"

    projects_dir = os.path.join(os.getcwd(), "projects")
    projects = os.listdir(projects_dir)

    for repo in projects:
        project_path = os.path.join(projects_dir, repo)
        logging.info("Counting .py files")
        file_count = count_files_recursive(project_path)
        logging.info(f"Found {file_count} .py files in {repo}")

        loader = GenericLoader.from_filesystem(
            project_path,
            glob="**/*",
            suffixes=[".py", ".md"],
            exclude=["**/non-utf8-encoding.py"],
            parser=LanguageParser(language=Language.PYTHON, parser_threshold=500),
        )
        documents = loader.load()
        logging.info(f"Loaded {len(documents)} documents from {repo}")

        splitter = RecursiveCharacterTextSplitter.from_language(
            language=Language.PYTHON, chunk_size=1000, chunk_overlap=100
        )
        texts = splitter.split_documents(documents)
        logging.info(f"Split into {len(texts)} text chunks for {repo}")

        underlying_embeddings = OpenAIEmbeddings(model=embeddings_model, api_key=api_key_openai)
        cache_store = LocalFileStore(os.path.join("cache", repo))
        embedder = CacheBackedEmbeddings.from_bytes_store(
            underlying_embeddings=underlying_embeddings,
            document_embedding_cache=cache_store,
            namespace="cache_embeddings_" + repo
        )
        db = Chroma.from_documents(texts, embedder)
        retriever = db.as_retriever(search_type="mmr", search_kwargs={"k": 8})

if __name__ == "__main__":
    main()