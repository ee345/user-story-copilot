from flask import Flask, jsonify, request, send_from_directory
import os
import tokeniser

from langchain.text_splitter import Language, RecursiveCharacterTextSplitter
from langchain_community.document_loaders.generic import GenericLoader
from langchain_community.document_loaders.parsers import LanguageParser
from langchain_core.messages import HumanMessage

from langchain.chains import ConversationalRetrievalChain
from langchain_mistralai import MistralAIEmbeddings
from langchain.memory import ConversationSummaryMemory
from langchain_openai import ChatOpenAI
from langchain_mistralai.chat_models import ChatMistralAI
from langchain.storage import LocalFileStore
from langchain_openai import OpenAIEmbeddings
from langchain_community.vectorstores import Chroma
from langchain.embeddings import CacheBackedEmbeddings


api_key_openai = os.getenv("OPENAI_API_KEY")
api_key_mistral = os.getenv("MISTRAL_API_KEY")
embeddings_model = "text-embedding-3-large"


app = Flask(__name__, static_folder='static')

@app.route('/')
def home():
    return send_from_directory(app.static_folder, 'index.html')


@app.route('/projects', methods=['GET'])
def get_projects():
    projects = os.listdir(os.getcwd() + "/projects/")
    return jsonify({
        "projects": projects
    })

@app.route('/process', methods=['POST'])
def process_request():
    request_data = request.json
    model_name = request_data['model']
    type_model = request_data['typeModelSelected']
    user_story = request_data['userDetailValue']
    repository_name = request_data['selectedRepo']

    project_path = os.path.join(os.getcwd(), "projects", repository_name)

    def count_python_files(directory):
        return sum(file.endswith(".py") for _, _, files in os.walk(directory) for file in files)

    python_files_count = count_python_files(project_path)
    print("Python files count:", python_files_count)

    document_loader = GenericLoader.from_filesystem(
        project_path,
        glob="**/*",
        suffixes=[".py", ".md"],
        exclude=["**/non-utf8-encoding.py"],
        parser=LanguageParser(language=Language.PYTHON, parser_threshold=500),
    )
    documents = document_loader.load()
    print(f"Loaded {len(documents)} documents")

    text_splitter = RecursiveCharacterTextSplitter.from_language(
        language=Language.PYTHON, chunk_size=1000, chunk_overlap=100
    )
    document_chunks = text_splitter.split_documents(documents)
    print(f"Split into {len(document_chunks)} chunks")

    embeddings = OpenAIEmbeddings(model=embeddings_model, api_key=api_key_openai)

    cache_directory = os.path.join(os.getcwd(), "cache", repository_name)
    embeddings_cache = LocalFileStore(cache_directory)
    cached_embeddings = CacheBackedEmbeddings.from_bytes_store(
        underlying_embeddings=embeddings,
        document_embedding_cache=embeddings_cache,
        namespace=f"cache_embeddings_{repository_name}"
    )
    vector_db = Chroma.from_documents(document_chunks, cached_embeddings)

    document_retriever = vector_db.as_retriever(search_type="mmr", search_kwargs={"k": 8})

    if type_model == "OpenAI":
        language_model = ChatOpenAI(model_name=model_name, api_key=api_key_openai, max_tokens=500)
    else:
        language_model = ChatMistralAI(model=model_name, mistral_api_key=api_key_mistral, max_tokens=500)

    conversation_memory = ConversationSummaryMemory(llm=language_model, memory_key="chat_history", return_messages=True)
    retrieval_chain = ConversationalRetrievalChain.from_llm(language_model, retriever=document_retriever, memory=conversation_memory)

    initial_question = ("As a Senior Software Engineer working on the project '" + repository_name + "', "
                        "your task is to identify all the specific locations within the project where the following issue can be addressed: "
                        "'" + user_story + "'. "
                        "The output should be a comprehensive list of all the actual locations in the project where the problem can be resolved. "
                        "It is crucial to base this on existing locations within the project without speculating or assuming hypothetical places.")
    initial_result = retrieval_chain(initial_question)

    incoming_tokens_count = tokeniser.estimate_tokens(initial_question)
    print("Incoming tokens count:", incoming_tokens_count)

    outgoing_tokens_count = tokeniser.estimate_tokens(initial_result["answer"])
    print("Outgoing tokens count:", outgoing_tokens_count)

    followup_question = ("You are a Senior Software Engineer working on the project named '" + repository_name + "'. "
                         "Your manager has provided the following details: '" + user_story + "'. "
                         "Your task is to craft a markdown document that outlines the ideal user story. "
                         "The user story should adhere to the valid markdown format, encapsulating: "
                         "1) A user story structured as ``` As [actor], I want [objective] so that [benefit]. ```"
                         "2) A brief background context (2-3 sentences only). "
                         "The acceptance criteria must be delineated as a checklist, employing Markdown-style checkboxes for clarity. "
                         "The tone of the text should be formal, befitting a professional environment, with a focus on precise technical specifics and unambiguous acceptance criteria. "
                         "It should steer clear of vague or broad statements and be prepared to request further details on unclear inputs to ensure the output's relevance and utility. "
                         "This task does not involve coding; it solely concerns creating a markdown document for issue descriptions on GitLab tickets. "
                         "Additional guidance for the initial task is provided as: '" + initial_result["answer"]) + "'."
    final_result = language_model.invoke([HumanMessage(content=followup_question)])
    print("Final answer:")
    print(final_result.content)

    incoming_tokens_count = incoming_tokens_count + tokeniser.estimate_tokens(followup_question)
    print("Incoming tokens count:", incoming_tokens_count)

    outgoing_tokens_count = outgoing_tokens_count + tokeniser.estimate_tokens(final_result.content)
    print("Outgoing tokens count:", outgoing_tokens_count)


    if final_result.content.startswith("```markdown"):
        final_result.content = final_result.content[11:]
    if final_result.content.endswith("```"):
        final_result.content = final_result.content[:-3]

    total_tokens_count = incoming_tokens_count + outgoing_tokens_count
    print("Total tokens count:", total_tokens_count)

    return jsonify({
        "message": "Data processed successfully",
        "request_data": request_data,
        "result": final_result.content,
        "token_counts": {
            "incoming": incoming_tokens_count,
            "outgoing": outgoing_tokens_count,
            "total": total_tokens_count
        }
    })


if __name__ == '__main__':
    app.run(debug=True)
