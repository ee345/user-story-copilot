document.addEventListener('DOMContentLoaded', () => {
    const elements = {
        userDetail: document.getElementById('userDetail'),
        userStoryInput: document.getElementById('userStoryInput'),
        markdownContent: document.getElementById('markdownContent'),
        typeModelSelect: document.getElementById('typeModel'),
        modelSelect: document.getElementById('model'),
        embeddingsTypeSelect: document.getElementById('embeddingsType'),
        generateStoryButton: document.getElementById('generateStory'),
        repoSelect: document.getElementById('repoSelect'),
        preloader: document.getElementsByClassName("preloader")[0]
    };

    const updateMarkdownPreview = () => {
        elements.markdownContent.innerHTML = marked.parse(elements.userStoryInput.value);
    };

    const models = {
        'OpenAI': ['gpt-4-0125-preview', 'gpt-4-turbo-preview', 'gpt-4-1106-preview	', 'gpt-4', 'gpt-4-0613', 'gpt-4-32k', 'gpt-4-32k-0613', 'gpt-3.5-turbo-0125', 'gpt-3.5-turbo', 'gpt-3.5-turbo-1106', 'gpt-3.5-turbo-instruct'],
        'MistralAI': ['mistral-tiny', 'mistral-small', 'mistral-medium']
    };

    const embeddings = {
        'OpenAI': ['text-embedding-3-large', 'text-embedding-3-small', 'text-embedding-ada-002'],
        'MistralAI': ['mistral-embed']
    };

    const projects = function() {
        fetch('/projects')
        .then(response => response.json())
        .then(data => {
            const options = data.projects.map(project => `<option value="${project}">${project}</option>`).join('');
            elements.repoSelect.innerHTML = options;
        });
    }
    projects();


    const updateOptions = (selectElement, options) => {
        selectElement.innerHTML = options.map(option => `<option value="${option}">${option}</option>`).join('');
    };

    const updateDropdowns = () => {
        updateOptions(elements.modelSelect, models[elements.typeModelSelect.value]);
        updateOptions(elements.embeddingsTypeSelect, embeddings[elements.typeModelSelect.value]);
    };

    elements.typeModelSelect.addEventListener('change', updateDropdowns);
    updateDropdowns();

    elements.userStoryInput.addEventListener('input', updateMarkdownPreview);

    const sendRequest = () => {
        const requestBody = {
            userDetailValue: elements.userDetail.value,
            selectedRepo: elements.repoSelect.value,
            typeModelSelected: elements.typeModelSelect.value,
            model: elements.modelSelect.value,
            embeddingsType: elements.embeddingsTypeSelect.value
        };

        if (!requestBody.userDetailValue) {
            alert('Please enter details for the user story');
            return;
        }

        elements.generateStoryButton.disabled = true;
        elements.preloader.style.display = "flex";

        fetch('/process', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(requestBody)
        })
        .then(response => response.json())
        .then(data => {
            elements.userStoryInput.value = data.result;
            updateMarkdownPreview();
            elements.generateStoryButton.disabled = false;
            elements.preloader.style.display = "none";
            document.getElementById('incomingTokens').textContent = data.token_counts.incoming;
            document.getElementById('outgoingTokens').textContent = data.token_counts.outgoing;
            document.getElementById('totalTokens').textContent = data.token_counts.total;
            const modelSelected = elements.modelSelect.value;
            const estimatedCost = calculateCost(modelSelected, data.token_counts.incoming, data.token_counts.outgoing);
            console.log("Estimated cost: ", estimatedCost);

        });
    };

    elements.generateStoryButton.addEventListener('click', sendRequest);
});