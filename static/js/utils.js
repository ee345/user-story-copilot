const calculateCost = (model, incomingTokens, outgoingTokens) => {
    let costPerToken;
    let currency;

    if (model.startsWith('mistral')) {
        currency = '€';
        switch (model) {
            case 'mistral-tiny':
                costPerToken = incomingTokens > 0 ? 0.14 / 1e6 : 0.42 / 1e6;
                break;
            case 'mistral-small':
                costPerToken = incomingTokens > 0 ? 0.6 / 1e6 : 1.8 / 1e6;
                break;
            case 'mistral-medium':
                costPerToken = incomingTokens > 0 ? 2.5 / 1e6 : 7.5 / 1e6;
                break;
            default:
                costPerToken = 0;
        }
    } else if (model.startsWith('gpt')) {
        currency = '$';
        switch (model) {
            case 'gpt-3.5-turbo-0125':
            case 'gpt-3.5-turbo-1106':
            case 'gpt-3.5-turbo-instruct':
            case 'gpt-3.5-turbo':
            case 'gpt-4-0125-preview':
            case 'gpt-4-1106-preview':
            case 'gpt-4-0613':
            case 'gpt-4-1106-vision-preview':
                costPerToken = incomingTokens > 0 ? 0.01 / 1e3 : 0.03 / 1e3;
                break;
            case 'gpt-4':
                costPerToken = incomingTokens > 0 ? 0.03 / 1e3 : 0.06 / 1e3;
                break;
            case 'gpt-4-32k-0613':
            case 'gpt-4-32k':
                costPerToken = incomingTokens > 0 ? 0.06 / 1e3 : 0.12 / 1e3;
                break;
            default:
                costPerToken = 0;
        }
    } else {
        currency = '$';
        costPerToken = 0;
    }
    const totalCost = (incomingTokens + outgoingTokens) * costPerToken;
    return `${currency}${totalCost.toFixed(8)}`;
};